<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('categoreis', 'Api\\CategoriesController@index');
Route::post('categories', 'Api\\CategoriesController@create');
Route::get('expenses', 'Api\\ExpensesController@index');
Route::get('expenses/chart', 'Api\\ExpensesController@chart');
