<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Expense;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();
        return $categories;
    }
    public function create(Request $request)
    {
        $expense = Expense::create([
            'title' => $request->title,
            'amount' => $request->amount,
            'category_id' => $request->category,
            'type' => $request->type == 'income' ? Expense::INCOME : Expense::OUTCOME
        ]);
        if ($expense) {
            return response()->json([
                'success' => true,
                'message' => 'عملیات با موفقیت انجام شد'
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'خطایی در عملیات رخ داده است'
        ]);
    }
}
