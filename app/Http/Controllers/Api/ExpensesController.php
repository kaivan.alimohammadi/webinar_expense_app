<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Expense;
use Illuminate\Support\Facades\DB;

class ExpensesController extends Controller
{
    public function index(Request $request)
    {
        $expenses = Expense::with('category')->get();
        return $expenses;
    }

    public function chart(Request $request)
    {
        $expenses = Expense::select(DB::raw('Date(`created_at`) as date,SUM(amount) as amount'))->groupBy(DB::raw('Date(created_at)'))->get();
        $lables = [];
        $values = [];
        foreach ($expenses as $expense) {
            $lables[] = $expense->date;
            $values[] = (int)$expense->amount;
        }
        $results = [];
        $results['labels'] = $lables;
        $results['values'] = $values;

        return $results;
    }
}
