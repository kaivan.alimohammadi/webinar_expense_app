<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    const INCOME = 1;
    const OUTCOME = 2;
    protected $guarded = [
        'id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getTypeAttribute($value)
    {
        if ($value == self::INCOME) {
            return 'درآمد';
        }
        return 'هزینه';
    }
}
