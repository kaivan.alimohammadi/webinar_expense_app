import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Application from './components/Application';
import { BrowserRouter as Router } from "react-router-dom";
if (document.getElementById('root')) {
    ReactDOM.render(<Router><Application /></Router>, document.getElementById('root'));
}
