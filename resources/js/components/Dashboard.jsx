import React, { Component } from 'react'
import { Container, Row, Col, Card, ListGroup } from 'react-bootstrap';
import { Link, Route, Switch } from "react-router-dom";

import List from './Expense/List';
import Add from './Expense/Add';
import Chart from './Chart';

export default class Dashboard extends Component {
    render() {
        return (
            <div id="appliction">
                <Container>
                    <Row>
                        <Col sm={4}>
                            <Card style={{ width: '18rem' }}>
                                <Card.Header>منو</Card.Header>
                                <ListGroup variant="flush">
                                    <ListGroup.Item>
                                        <Link to="/expenses/list">لیست هزینه ها</Link>
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        <Link to="/expenses/add">اضافه کردن </Link>
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        <Link to="/expenses/chart">نمودار</Link>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card>
                        </Col>
                        <Col sm={8}>
                            <Card>
                                <Card.Header>محتوا</Card.Header>
                                <Card.Body>
                                    <Switch>
                                        <Route path="/expenses/list" component={List} />
                                        <Route path="/expenses/add" component={Add} />
                                        <Route path="/expenses/chart" component={Chart} />
                                    </Switch>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}
