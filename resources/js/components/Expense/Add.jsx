import React, { Component } from 'react'
import { Form, Button, Alert } from 'react-bootstrap';
export default class Add extends Component {

    constructor() {
        super();
        this.state = {
            categories: [],
            showMessage: false,
            messageType: '',
            message: ''
        };
    }
    componentDidMount() {
        window.axios.get('/api/categoreis')
            .then(response => {
                this.setState({
                    categories: response.data
                });
            })
            .catch(error => { });
    }
    saveExpense = (e) => {
        e.preventDefault();
        window.axios.post('/api/categories', {
            title: document.querySelector('#title').value,
            category: document.querySelector('#category').value,
            amount: document.querySelector('#amount').value,
            type: document.querySelector('#expense_income').checked ? 'income' : 'outcome'
        }).then(response => {
            let { success, message } = response.data;
            this.setState({
                showMessage: true,
                messageType: success,
                message
            });
            document.querySelector('#add_frm_expense').reset();
        }).catch(error => {
            console.log(error);

        });
    }
    render() {
        let { categories } = this.state;
        let messages = this.state.showMessage ? <Alert variant={this.state.messageType ? 'success' : 'danger'}>{this.state.message}</Alert> : '';
        let categoreisAsOptions = categories.map(category => <option key={category.id} value={category.id}>{category.name}</option>);
        return (
            <React.Fragment>
                {messages}
                <Form id="add_frm_expense" onSubmit={(e) => { this.saveExpense(e) }}>
                    <Form.Group controlId="title">
                        <Form.Label>عنوان</Form.Label>
                        <Form.Control type="text" placeholder="عنوان هزینه را وارد کنید" />
                    </Form.Group>
                    <Form.Group controlId="category">
                        <Form.Label>دسته بندی:</Form.Label>
                        <Form.Control as="select">
                            {categoreisAsOptions}
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="amount">
                        <Form.Label>مبلغ</Form.Label>
                        <Form.Control type="number" placeholder="مبلغ را وارد کنید" />
                    </Form.Group>
                    <div className="form-check">
                        <input className="form-check-input" name="expense_type" type="radio" value="income" id="expense_income" defaultChecked />
                        <label className="form-check-label" htmlFor="expense_income">
                            درآمد
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" name="expense_type" type="radio" value="outcome" id="expense_outcome" />
                        <label className="form-check-label" htmlFor="expense_outcome">
                            هزینه
                        </label>
                    </div>
                    <Button variant="primary" type="submit">ذخیره اطلاعات</Button>
                </Form>
            </React.Fragment>
        )
    }
}
