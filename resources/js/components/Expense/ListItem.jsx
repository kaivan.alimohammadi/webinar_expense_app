import React from 'react'

export default function ListItem({ item }) {
    console.log(item);
    return (
        <tr>
            <td>{item.title}</td>
            <td>{item.category.name}</td>
            <td>{item.type}</td>
            <td>{item.amount}</td>
        </tr>
    )
}
