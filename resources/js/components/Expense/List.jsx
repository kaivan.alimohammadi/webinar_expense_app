import React, { Component } from 'react'

import { Table } from 'react-bootstrap';
import ExpenseItem from './ListItem';
export default class List extends Component {

    constructor() {
        super();
        this.state = {
            expenses: []
        };
    }
    componentDidMount() {
        window.axios.get('/api/expenses')
            .then(response => {
                this.setState({
                    expenses: response.data
                });
            })
            .catch(error => { });
    }
    render() {
        let rows = this.state.expenses.map(expense => <ExpenseItem key={expense.id} item={expense} />);
        return (
            <div id="expenses">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>عنوان</th>
                            <th>دسته بندی</th>
                            <th>نوع</th>
                            <th>مبلغ</th>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </Table>
            </div>
        )
    }
}
