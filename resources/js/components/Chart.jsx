import React, { Component } from 'react'

import { Line } from "react-chartjs-2";

export default class Chart extends Component {

    constructor() {
        super();
        this.state = {
            labels: null,
            values: null,
            isLoaded: false
        };
    }
    componentDidMount() {
        window.axios.get('/api/expenses/chart')
            .then(response => {

                this.setState((prevState) => {
                    return {
                        labels: response.data.labels,
                        values: response.data.values,
                        isLoaded: true
                    }
                });
            })
            .catch(error => {
                console.log(error)
            })
    }
    render() {
        let data = {
            labels: this.state.labels,
            datasets: [
                {
                    label: 'My First dataset',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.state.values
                }
            ]
        };
        let chartToRender = this.state.isLoaded ? <Line data={data} width={600} height={400} /> : '';
        return (
            <div>
                {chartToRender}
            </div>
        )
    }
}
